import Layout from "../components/Layout";
import ForgotPage from "../components/ForgotPage";

const forgotpassword = () => {
  return (
    <Layout pageTitle="Forgot password">
      <ForgotPage />
    </Layout>
  );
};

export default forgotpassword;
