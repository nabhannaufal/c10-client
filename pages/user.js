import Profil from "../components/Profil";
import HeaderUser from "../components/HeaderUser";
import Layout from "../components/Layout";
import ListGame from "../components/ListGame";
import Suwit from "../components/Suwit";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { userAction } from "../redux/actions";

const User = ({ getProfil }) => {
  const router = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    const username = Cookies.get("username");
    const login = Cookies.get("login");
    if (!login) {
      return router.push("/login");
    }
    dispatch(userAction(username));
  }, [router, dispatch]);

  return (
    <Layout pageTitle="Dashboard">
      <HeaderUser />
      <Profil />
      <ListGame />
      <Suwit />
    </Layout>
  );
};

export default User;
