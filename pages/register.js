import Layout from "../components/Layout";
import RegisterPage from "../components/RegisterPage";

const forgotpassword = () => {
  return (
    <Layout pageTitle="Forgot password">
      <RegisterPage />
    </Layout>
  );
};

export default forgotpassword;
