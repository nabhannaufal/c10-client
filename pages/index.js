import Layout from "../components/Layout";
import Header from "../components/Header";
import Hero from "../components/Hero";
import TheGame from "../components/TheGame";
import Feature from "../components/Feature";
import Requirement from "../components/Requirement";
import TopScore from "../components/TopScore";
import NewsLetter from "../components/NewsLetter";

const Index = () => {
  return (
    <Layout pageTitle="Landing Page Nextjs">
      <Header />
      <Hero />
      <TheGame />
      <Feature />
      <Requirement />
      <TopScore />
      <NewsLetter />
    </Layout>
  );
};
export default Index;
