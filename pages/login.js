import Layout from "../components/Layout";
import LoginPage from "../components/LoginPage";

const login = () => {
  return (
    <Layout pageTitle="Login">
      <LoginPage />
    </Layout>
  );
};

export default login;
